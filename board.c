/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "board.h"

Cell BOARD_1[BOARD_HEIGHT][BOARD_WIDTH] =
{
    { BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY },
    { EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, BLOCKED, EMPTY, BLOCKED, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED }
};

Cell BOARD_2[BOARD_HEIGHT][BOARD_WIDTH] =
{
    { BLOCKED, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, EMPTY, EMPTY, EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
    { EMPTY, BLOCKED, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY }
};
void initialiseBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH])
{
	int i;
    int j;
    for(i=0;i<BOARD_HEIGHT;++i){
    	for(j=0;j<BOARD_WIDTH;++j){
    		board[i][j]=EMPTY;
		}
	}
	printf("| ");
	for(j=0;j<BOARD_WIDTH;j++){
		printf("|%d",j);
	}
	for(i=0;i<BOARD_HEIGHT;++i){
		printf("\n|%d",i);		   	
    	for(j=0;j<BOARD_WIDTH;++j){			
				printf("| ");	
							  					
		}
		
	}
}

void loadBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH],
               Cell boardToLoad[BOARD_HEIGHT][BOARD_WIDTH])
{
    int i;
    int j;
    for(i=0;i<BOARD_HEIGHT+1;++i){
    	for(j=0;j<BOARD_HEIGHT+1;++j){
    		board[i][j]=boardToLoad[i][j];
		}
	}
}

Boolean placePlayer(Cell board[BOARD_HEIGHT][BOARD_WIDTH], Position position)
{
   	if(board[position.y][position.x]==EMPTY){
   		board[position.y][position.x]=PLAYER;
		return TRUE;		
	}
    return FALSE;
}

PlayerMove movePlayerForward(Cell board[BOARD_HEIGHT][BOARD_WIDTH],
                             Player * player)
{
    /* TODO */
    return PLAYER_MOVED;
}

void displayBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH], Player * player)
{

	int i;
	int j;
	printf("| ");
	for(j=0;j<BOARD_WIDTH;j++){
		printf("|%d",j);
	}
	for(i=0;i<BOARD_HEIGHT;++i){
		printf("\n|%d",i);   	
    	for(j=0;j<BOARD_WIDTH;++j){
				
    		printf("|");
    		if(board[i][j]==BLOCKED){
				printf("*");
			}
			else if(board[i][j]==EMPTY){
				printf(" ");
			}
			else if(board[i][j]==PLAYER){
				if(player->direction==NORTH){
					printf(DIRECTION_ARROW_OUTPUT_NORTH);
				}
				else if(player->direction==SOUTH){
					printf(DIRECTION_ARROW_OUTPUT_SOUTH);
				}
				else if(player->direction==EAST){
					printf(DIRECTION_ARROW_OUTPUT_EAST);
				}
				else if(player->direction==WEST){
					printf(DIRECTION_ARROW_OUTPUT_WEST);
				}
			}
						
		}
		
	}
}
