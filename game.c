/******************************************************************************
** Student name: 	Laurensius Frederik Tanno
** Student number: 	3702759
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "game.h"

/*
	This section contains the code for displaying the menu and instructions for playing the game
	as well as for loading the board. 
*/
void playGame()
{ 
	initialiseBoard(board);   
	flag=0;	
	while(flag==0){				
		inputFunction(input);				
		token=strtok(input," ");
		while(1==1){
			if(strcmp(token,"load")==0){/*check user input for 'load' command*/
				ret=loadCommand(token,board);
				if(ret==2){
					printf("\n We only have 2 boards\n");
					break;
				}
				else if(ret==-3){
					break;
				}
				
				break;
			}
			
			if(strcmp(token,"init")==0){
				
				token = strtok(NULL," ");
				pstn = (Position *)malloc(sizeof(Position));
				plyr=(Player *)malloc(sizeof(Player));
				dir=readCommands(token,coord,dir);
				if(dir==-1 || dir==-2 || dir==-3){
					errorMessage();
					/*inputFunction(input);*/
					break;
				}
				
				pstn->x=coord[0];
				pstn->y=coord[1];
					
				initialisePlayer(plyr,pstn,dir);		
				if(placePlayer(board,(*pstn))==FALSE){
					printf("\nThe road is blocked\n");
					break;
				}	
				displayBoard(board,plyr);
				
				while(1==1){
					inputFunction(input);
					token=strtok(input," ");
					
					if(strcmp(token,"forward")==0 || strcmp(token,"f")==0){
						checkdir=checkDir(plyr,board);
						if(checkdir==-1){
							printf("\nYou have reached the edge of the board. You cannot proceed any further\n");
							
						}
						else if(checkdir==2){
							printf("\nThe road is blocked. Please find another way\n");
							
						}
						else if(checkdir==0){
							moves++;
						}
						
						
					}
					else if(strcmp(token,"turn_left")==0 || strcmp(token,"left")==0){
						turn=TURN_LEFT;
						turnDirection(plyr,turn);
					}
					else if(strcmp(token,"turn_right")==0 || strcmp(token,"right")==0){
						turn=TURN_RIGHT;
						turnDirection(plyr,turn);
					}
					else if(strcmp(input,"quit")==0){
						
						printf("\nSuccessful moves: %d",moves);
						return;
					}
					else{
						errorMessage();
					}
					displayBoard(board,plyr);
				}
			}	
			else if(strcmp(input,"quit")==0){
				flag=1;
				printf("\nSuccessful moves: %d",moves);
				break;
			}
			else{
				errorMessage();
				token=NULL;
				break;	
			}
		}
	if(flag==1){
		break;
	}			
	token=NULL;		
	}
}

int loadCommand(char *token, Cell board[BOARD_HEIGHT][BOARD_WIDTH]){
	while(token!=NULL){
		token=strtok(NULL," ");
		if(token==NULL){
			errorMessage();
			return -3;
		}		
		else if(strcmp(token,"1")==0){
			loadBoard(board, BOARD_1);
			displayBoard(board, NULL);
			return 0;
		}
				
		else if(strcmp(token,"2")==0){
			loadBoard(board, BOARD_2);
			displayBoard(board, NULL);
			return 0;
		}		
		else{
			errorMessage();
	     	return 2;
		}									
	}
	return 0;	
}

int checkDir(Player *plyr, Cell board[BOARD_HEIGHT][BOARD_WIDTH]){
	Position pstn=getNextForwardPosition(plyr);
	if(pstn.y==-1 || pstn.x==-1 || pstn.y==10 || pstn.x==10){
		return -1;
	}
	else if(board[pstn.y][pstn.x]==BLOCKED){
		return 2;
	}
	board[plyr->position.y][plyr->position.x]=EMPTY;
	updatePosition(plyr,pstn);
	placePlayer(board,pstn);
	return 0;
}
