/******************************************************************************
** Student name: 	Laurensius Frederik Tanno
** Student number: 	3702759
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "carboard.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
    char *cache=NULL;
    int i;
    while(1==1){
    	printf("\nWelcome to Carboard");
    	printf("\n-------------------");
    	printf("\n1. Play Game");
    	printf("\n2. Show Student Information");
    	printf("\n3. Quit");
    	printf("\nPlease enter your choice ");
    	fgets(input,30,stdin);
    	i=strtol(input,&cache,10);
    	clearBuffer(input);
    	if(i==1){
    		
    		playGame();
		}
    	else if(i==2){
    		showStudentInformation();
		}
		else if(i==3){
			printf("Good bye! \n\n");
			getchar();			
			return EXIT_SUCCESS;
		}
		else{
			printf("\nInvalid Input. Please try again\n");
			
		}
		
	}	
    return EXIT_SUCCESS;
}

void showStudentInformation()
{
    printf("\n\n");
   	printf("Student Name: Laurensius Frederik Tanno");		
	printf("\nStudent Number: 3702759");
    printf("\nEmail: s3702759@student.rmit.edu.au\n");
}

void viewInstructions(){
		printf("\n\n");
		printf("You can use the following commands to play the game:\n");
		printf("load <g>\n");
		printf("\tg: number of the game board to load\n");
		printf("init <x>,<y>,<direction>");
		printf("\n\tx: horizontal position of the car on the board (between 0 & 9)");
		printf("\n\ty: vertical position of the car on the board (between 0 & 9)");
		printf("\n\tdirection: direction of the car's movement (north, east, south, west)");
		printf("\nforward (or f)");
		printf("\nturn_left (or l)");
		printf("\nturn_right (or r)");
		printf("\nquit\n");
}
