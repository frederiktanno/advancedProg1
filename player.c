/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "player.h"

void initialisePlayer(Player * player, Position * position, Direction direction)
{
    player->direction=direction;
    player->position.x= position->x;
    player->position.y=position->y;
}

void turnDirection(Player * player, TurnDirection turnDirection)
{
    if(turnDirection==TURN_LEFT && player->direction==NORTH){
    	player->direction=WEST;
	}
	else if(turnDirection==TURN_LEFT && player->direction==SOUTH){
    	player->direction=EAST;
	}
	else if(turnDirection==TURN_LEFT && player->direction==WEST){
		player->direction=SOUTH;
	}
	else if(turnDirection==TURN_LEFT && player->direction==EAST){
		player->direction=NORTH;
	}
	else if(turnDirection==TURN_RIGHT && player->direction==NORTH){
		player->direction=EAST;
	}
	else if(turnDirection==TURN_RIGHT && player->direction==SOUTH){
		player->direction=WEST;
	}
	else if(turnDirection==TURN_RIGHT && player->direction==WEST){
		player->direction=NORTH;
	}
	else if(turnDirection==TURN_RIGHT && player->direction==EAST){
		player->direction=NORTH;
	}
}

Position getNextForwardPosition(const Player * player)
{
    Position position=player->position;
    if(player->direction==NORTH){
    	position.y--;
	}
	else if(player->direction==SOUTH){
		position.y++;
	}
	else if(player->direction==EAST){
		position.x++;
	}
	else if(player->direction==WEST){
		position.x--;
	}	
    return position;
}

void updatePosition(Player * player, Position position)
{
    player->position.x=position.x;
    player->position.y=position.y;
}

void displayDirection(Direction direction)
{
    /* TODO */
}
