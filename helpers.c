/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "helpers.h"
#include "game.h"
int readCommands(char *token,int coord[INIT_COORD],Direction dir)
{
	char *cache;
	int i=0;    
	token=strtok(token,",");
	
    while(i < 3){
		if(token==NULL){
			return -1;
		}    	
		if(i==0){
			coord[i]=(int)strtol(token,&cache,10);
			if(coord[i]<0||coord[i]>=10){	
				return -2;
			}
		}
		else if(i==1){
			token = strtok(NULL,",");
			coord[i]=(int)strtol(token,&cache,10);
			if(coord[i]<0||coord[i]>=10){
				return -3;
			}
		}
		else if(i==2){
			token = strtok(NULL,",");
			if(strcmp(token,DIRECTION_NORTH)==0){
				dir=NORTH;
			}
			else if(strcmp(token,DIRECTION_SOUTH)==0){
				dir=SOUTH;
			}
			else if(strcmp(token,DIRECTION_EAST)==0){
				dir=EAST;
			}
			else if(strcmp(token,DIRECTION_WEST)==0){
				dir=WEST;
			}
			else{
				return -1;	
			}
		}   	
    	i++;
	}

	return dir;
}

void clearBuffer(char input[INPUT_BUFFER]){
	int i=0;
	for(i=0;i<INPUT_BUFFER;i++){
		input[INPUT_BUFFER]=' ';
	}
}
	

void readRestOfLine(){
	int ch=0;
    while(ch = getc(stdin), ch != EOF && ch != '\n')
    { } /* Gobble each character. */

    /* Reset the error status of the stream. */
    clearerr(stdin);
}

void clearNewLine(char input[INPUT_BUFFER]){
	input[strcspn(input,"\n")]=0;
}

void errorMessage(){
	printf("\nInvalid command. Please try again\n");
}

void inputFunction(char input[INPUT_BUFFER]){
	clearBuffer(input);
	viewInstructions();		
	fgets(input,30,stdin);
	clearNewLine(input);
}
