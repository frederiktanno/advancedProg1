/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef HELPERS_H
#define HELPERS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include "player.h"


typedef enum boolean
{
    FALSE = 0,
    TRUE
} Boolean;

#define NEW_LINE_SPACE 1
#define NULL_SPACE 1
#define INPUT_BUFFER 30
#define INIT_COORD 2

char input[INPUT_BUFFER];

/**
 * This is used to compensate for the extra character spaces taken up by
 * the '\n' and '\0' when input is read through fgets().
 **/
#define EXTRA_SPACES (NEW_LINE_SPACE + NULL_SPACE)

#define EMPTY_STRING ""

/**
 * Call this function whenever you detect buffer overflow but only call this
 * function when this has happened.
 **/

void clearNewLine(char input[INPUT_BUFFER]);

void clearBuffer(char input[INPUT_BUFFER]);

int readCommands(char *token,int coord[INIT_COORD],Direction dir);

void readRestOfLine();

void errorMessage();

void inputFunction(char input[INPUT_BUFFER]);
#endif
